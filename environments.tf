module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  providers = {
    scaleway = scaleway.scaleway_project
  }
}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  providers = {
    scaleway = scaleway.scaleway_project
  }
}
