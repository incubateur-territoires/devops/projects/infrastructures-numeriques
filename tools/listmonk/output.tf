output "user" {
  value     = random_string.default_username.result
  sensitive = true
}
output "password" {
  value     = random_password.default_password.result
  sensitive = true
}
