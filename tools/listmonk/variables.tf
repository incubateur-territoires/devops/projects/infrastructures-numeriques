variable "scaleway_access_key" {
  type      = string
  sensitive = true
}
variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}
variable "hostname" {
  type = string
}

variable "base_domain" {
  type = string
}

variable "monitoring_org_id" {
  type      = string
  sensitive = true
}
variable "namespace" {
  type = string
}
