locals {
  s3_backup_region      = "fr-par"
  s3_backup_bucket_name = "infranum-listmonk-prod-backups-databases"
}

resource "scaleway_object_bucket" "backups" {
  name   = local.s3_backup_bucket_name
  region = local.s3_backup_region
}

module "postgresql" {
  source         = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version        = "0.0.22"
  namespace      = module.namespace.namespace
  chart_name     = "postgresql"
  pg_replicas    = 2
  pg_volume_size = "5Gi"

  pg_backups_volume_enabled             = true
  pg_backups_volume_size                = "10Gi"
  pg_backups_volume_full_schedule       = "5 4 * * 0"
  pg_backups_volume_incr_schedule       = "5 4 * * 1-6"
  pg_backups_volume_full_retention      = 4
  pg_backups_volume_full_retention_type = "count"

  pg_backups_s3_enabled       = true
  pg_backups_s3_bucket        = scaleway_object_bucket.backups.name
  pg_backups_s3_region        = scaleway_object_bucket.backups.region
  pg_backups_s3_endpoint      = "s3.${scaleway_object_bucket.backups.region}.scw.cloud"
  pg_backups_s3_access_key    = var.scaleway_access_key
  pg_backups_s3_secret_key    = var.scaleway_secret_key
  pg_backups_s3_full_schedule = "35 4 * * 0"
  pg_backups_s3_incr_schedule = "35 4 * * 1-6"
  values = [
    file("${path.module}/db_resources.yaml"),
    <<-EOT
      imagePgBackRest: null
      imagePostgres: null
      postgresVersion: 14
    EOT
  ]
}
