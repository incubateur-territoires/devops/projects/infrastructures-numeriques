module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  max_cpu_requests  = 2
  max_memory_limits = "8Gi"
  namespace         = var.namespace
  project_name      = "Listmonk"
  project_slug      = "listmonk"

  default_container_cpu_requests  = "30m"
  default_container_memory_limits = "32Mi"
}

resource "random_string" "default_username" {
  length  = 32
  special = false
}

resource "random_password" "default_password" {
  length = 128
}

resource "scaleway_object_bucket" "media" {
  name   = "infranum-listmonk-prod-media"
  region = local.s3_backup_region
}

resource "scaleway_object_bucket_acl" "media" {
  bucket = scaleway_object_bucket.media.name
  acl    = "public-read"
}

resource "scaleway_object_bucket_website_configuration" "media" {
  bucket = scaleway_object_bucket.media.name
  index_document {
    suffix = "index.html"
  }
}

module "listmonk" {
  source  = "gitlab.com/vigigloo/tools-k8s/listmonk"
  version = "0.0.1"

  chart_name    = "listmonk"
  chart_version = "0.1.6"
  namespace     = module.namespace.namespace

  values = [
    <<-EOT
    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}
    default_admin:
      username: ${yamlencode(random_string.default_username.result)}
      password: ${yamlencode(random_password.default_password.result)}
    database:
      username: ${yamlencode(module.postgresql.user)}
      password: ${yamlencode(module.postgresql.password)}
      name: ${yamlencode(module.postgresql.dbname)}
      host: ${yamlencode(module.postgresql.host)}
      SSL: "require"
      service:
        port: ${yamlencode(module.postgresql.port)}
    resources:
      limits:
        memory: 250Mi
      requests:
        cpu: 100m
    ingress:
      enabled: true
      className: null
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
      hosts:
        - host: ${var.hostname}
          paths:
            - path: /
              pathType: Prefix
      tls:
        - hosts:
            - ${var.hostname}
          secretName: listmonk-tls
    EOT
  ]
}
