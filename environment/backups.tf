locals {
  db_backups        = contains(["production", "development"], var.gitlab_environment_scope)
  db_backups_switch = local.db_backups ? 1 : 0
}
resource "scaleway_object_bucket" "backup" {
  count = local.db_backups_switch
  name  = "mongodb-${var.gitlab_environment_scope}-backups"
}
