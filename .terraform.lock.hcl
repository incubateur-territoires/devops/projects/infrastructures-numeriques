# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/gitlabhq/gitlab" {
  version     = "3.12.0"
  constraints = "~> 3.12.0"
  hashes = [
    "h1:zZKpUsf1STvLLyH30a6ZcjQoARU8MJGbHsGCjYHrnOA=",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:207f7ffaaf16f3d0db9eec847867871f4a9eb9bb9109d9ab9c9baffade1a4688",
    "zh:2360bdd3253a15fbbb6581759aa9499235de670d13f02272cbe408cb425dc625",
    "zh:2a9bec466163baeb70474fab7b43aababdf28b240f2f4917299dfe1a952fd983",
    "zh:3a6ea735b5324c17aa6776bff3b2c1c2aeb2b6c20b9c944075c8244367eb7e4c",
    "zh:3aa73c622187c06c417e1cd98cd83f9a2982c478e4f9f6cd76fbfaec3f6b36e8",
    "zh:51ace107c8ba3f2bb7f7d246db6a0428ef94aafceca38df5908167f336963ec8",
    "zh:53a35a827596178c2a12cf33d1f0d0b4cf38cd32d2cdfe2443bdd6ebb06561be",
    "zh:5bece68a724bffd2e66293429319601d81ac3186bf93337c797511672f80efd0",
    "zh:60f21e8737be59933a923c085dcb7674fcd44d8a5f2e4738edc70ae726666204",
    "zh:9fb277f13dd8f81dee7f93230e7d1593aca49c4788360c3f2d4e42b8f6bb1a8f",
    "zh:ac63a9b4a3a50a3164ee26f1e64cc6878fcdb414f50106abe3dbeb7532edc8cd",
    "zh:ed083de9fce2753e3dfeaa1356508ecb0f218fbace8a8ef7b56f3f324fff593b",
    "zh:ed966026c76a334b6bd6d621c06c5db4a6409468e4dd99d9ad099d3a9de22768",
  ]
}

provider "registry.opentofu.org/grafana/grafana" {
  version     = "1.27.0"
  constraints = "~> 1.27.0"
  hashes = [
    "h1:ifKgRrZHspeXMSavSCsWbLrk6tC1NOyWN0iiGv2j6I4=",
    "zh:01ef0ae20530a54cbb4bffc35e97733916e5ae2e8f7fa00aefa2e86e24206823",
    "zh:08a4ac8b690bab9a3b454c3d998917f4ed49fc225a21ff53ceb0488eb4b9d15d",
    "zh:0e08516cd6c2495bc83a4a8e0252bfa70e310aa400af0fe766bbe7ddd05a21cb",
    "zh:14856865f6e6695e6d7708d70844a2c031cfc9b091e7cf530a453b2f78c9a691",
    "zh:2b1c05fff5011ab83acdd292484857fe886cd113abbb7fc617bbb8f358517cc0",
    "zh:31bae1b1c635a94329470b30986d336f4b3819bf24aacd953d5b57debb83bd4d",
    "zh:352b6ea190711c8f3f107540c8943c8f6b9faf4fbc73a9c1721b15db4a103edb",
    "zh:7eda29d30d451b842c5b0b2cf15cb907e76e8bac4843e90830a62a68bbe877a5",
    "zh:bd640d7e8a126d810a34766816b4e17a07c634ffef14b468269c8191683fff27",
    "zh:ddfa43a7b31fb840f04420c82fe0313a44fa5099c3d1f61219e630d6c8440e2d",
    "zh:e50dccaf8cb9922ac25e2f87a85083d5c2cef5323eac4ce7d933012af7a25e88",
    "zh:e72903aeb4830b7b89efcf7336a61c736d9049c4156b6f17cec51663ed6e803d",
    "zh:f4161d62960ec9f9d84cb73437a9b9195831c467cdcc3381e431fa6e2cd92a14",
    "zh:f4699da872dfc9847eb3da49fd6ae4943e92602b617931bb07b91e646d90a279",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.14.0"
  constraints = "~> 2.0"
  hashes = [
    "h1:ibK3MM61pVjBwBcrro56OLTHwUhhNglvGG9CloLvliI=",
    "zh:1c84ca8c274564c46497e89055139c7af64c9e1a8dd4f1cd4c68503ac1322fb8",
    "zh:211a763173934d30c2e49c0cc828b1e34a528b0fdec8bf48d2bb3afadd4f9095",
    "zh:3dca0b703a2f82d3e283a9e9ca6259a3b9897b217201f3cddf430009a1ca00c9",
    "zh:40c5cfd48dcef54e87129e19d31c006c2e3309ee6c09d566139eaf315a59a369",
    "zh:6f23c00ca1e2663e2a208a7491aa6dbe2604f00e0af7e23ef9323206e8f2fc81",
    "zh:77f8cfc4888600e0d12da137bbdb836de160db168dde7af26c2e44cf00cbf057",
    "zh:97b99c945eafa9bafc57c3f628d496356ea30312c3df8dfac499e0f3ff6bf0c9",
    "zh:a01cfc53e50d5f722dc2aabd26097a8e4d966d343ffd471034968c2dc7a8819d",
    "zh:b69c51e921fe8c91e38f4a82118d0b6b0f47f6c71a76f506fde3642ecbf39911",
    "zh:fb8bfc7b8106bef58cc5628c024103f0dd5276d573fe67ac16f343a2b38ecee8",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.31.0"
  constraints = ">= 2.13.0, ~> 2.13"
  hashes = [
    "h1:z2qlqn6WbrjbezwQo4vvlwAgVUGz59klzDU4rlYhYi8=",
    "zh:0dd25babf78a88a61dd329b8c18538a295ea63630f1b69575e7898c89307da39",
    "zh:3138753e4b2ce6e9ffa5d65d73e9236169ff077c10089c7dc71031a0a139ff6d",
    "zh:644f94692dc33de0bb1183c307ae373efbf4ef4cb92654ccc646a5716edf9593",
    "zh:6cc630e43193220b1599e3227286cc4e3ca195910e8c56b6bacb50c5b5176dbf",
    "zh:764173875e77aa482da4dca9fec5f77c455d028848edfc394aa7dac5dfed6afd",
    "zh:7b1d380362d50ffbb3697483036ae351b0571e93b33754255cde6968e62b839f",
    "zh:a1d93ca3d8d1ecdd3b69242d16ff21c91b34e2e98f02a3b2d02c908aeb45189b",
    "zh:b471d0ab56dbf19c95fba68d2ef127bdb353be96a2be4c4a3dcd4d0db4b4180a",
    "zh:d610f725ded4acd3d31a240472bb283aa5e657ed020395bdefea18d094b8c2bf",
    "zh:d7f3ddd636ad5af6049922f212feb24830b7158410819c32073bf81c359cd2fa",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.3.2"
  constraints = "~> 3.3.2"
  hashes = [
    "h1:3cVQP2pZt7WXSmZVYRj75U8PTP17Tbo+LubWAJJvt6s=",
    "zh:06940c8bc66b49e27c4a7030242df2211be2635bc061b656c9110b521f0d6f71",
    "zh:0aa9d7f1d7971b662485ca2474fc13ab2dba7951ad56f37d08ccf92c2e918bec",
    "zh:0e265e4154792c79865c27c55661f63c3df56e9ab961f47c4014f255e4aa3c33",
    "zh:2d8305ed9ddd1907b81a208170c7599ffb99eacf2639cf40b5c7fe384585ee87",
    "zh:43c7dd999908ead0e98a053c294b7d53546c45d6317e9124df39f6ed31e5fce8",
    "zh:5ddce4cb91ddda675071166d975cc5af2ccb5efabbf327e9e5d21f0b93c9ab6c",
    "zh:777ef4bba1f1875c4bcda9c2207bb00a24758a2a6c9097446c84d7cc20356673",
    "zh:81542311f3d1fac213c9c25d3650de2a0d54cc480ec9c5abc16d88f9802b82b0",
    "zh:9031150598307c66e61c13f7ca7b750ff8c4e373dc912f869d8ca81f9d1b4a2e",
    "zh:9c8caf2248dadd21480bd2705680d76e9939f2b5956b6863789bbe0ec5457892",
  ]
}

provider "registry.opentofu.org/hashicorp/time" {
  version     = "0.11.2"
  constraints = ">= 0.7.0"
  hashes = [
    "h1:WVXqTGyMwPRVhT7R9DAMhTwdNaoAQeuGKqAncyS5yh8=",
    "zh:534d56fac2daa5c15737fa1907b7afe2d91474197a0ec272d3f864d2a0a87743",
    "zh:67c7765dc3b5ec19b6df32c29620de4b3e7846f5aa1a6b1b0e15394d87d0f875",
    "zh:9502412f028d17051ea0550f5015d0515da78bc938f415191d9171742481330c",
    "zh:982984971fe6cb87600f118fbf754e7f60f7ad343735a4f99d632bf148319cee",
    "zh:9b0bedf8781be6c414f314989246a3b9a79302845db57dbe497d768cd56ee73e",
    "zh:a35244d464dcad8940060c49b66a2f653d2dfc4e471c2dd59e2118a17a279427",
    "zh:c8e3534b8cdff62f88a9882aed8c6dc4484639662bff2ea4e89499f1209b9ba9",
    "zh:e78c88613bb6eab52d94bb7592d4df3a79bf69b9f1e50a86f9d9174db7e5251c",
    "zh:fb28ffd2b641a449bd5526bef6e547894b4c5ddac0cef05ee03881a6c53eac39",
    "zh:fcfcd35ff1178cbf11034e2eaf4254f9aadd8531f75d1b15b5e245715183d49b",
  ]
}

provider "registry.opentofu.org/scaleway/scaleway" {
  version     = "2.17.0"
  constraints = "~> 2.17.0"
  hashes = [
    "h1:tDvtORFKFc8dfPSn54DmfzNhYSMgAgF3o7ByMshLknk=",
    "zh:27073d895debe582bfc537b251e755096d62d6f0465bc1adb3bcb23509032231",
    "zh:291650b711fad9aaaaf73b62e84dd4e03b43357abd5fb2e6aa15479181596b92",
    "zh:424736e7108bc69b59ab176b82265ab2ae1b6770c938a21fa35d38d8bd81a0dc",
    "zh:4ad06c08964f1074d313ab08524a209efc8af2af9da43372cf2006f2a3fe2990",
    "zh:6621c3664f1f8a49ca1e2df2d9e44422037e339790a72892446b1200e2dee18d",
    "zh:7c79dfdeda0202093e9a59cb58b2fc1d9dd76aaf9365f62e2df34b7e717ef411",
    "zh:7ca107bbd0933942c146b33cc387bf008008ab2294e9d9cd2e0a3f9c9cbb341a",
    "zh:80604402abcb6d3393208fb8b735bf77679af6e1fc060301058d711428eebccb",
    "zh:89188a3997bd46898e66a35ed349fcfc1e6607f5a747353f1be81126814ae5fd",
    "zh:8baa5c3ecc1926dbceb0d1a9361ae5c4300fa45d10ac8cd69af147739540adb5",
    "zh:931d6bdd5beb78d59345db4dc609c72f6466508f598f21047c82dd2c624a7a09",
    "zh:a6c59387cbc94afc47bc65536e98646d61426c0231e273649949630828ce1f3d",
    "zh:bab11d420bf928365768215b38d491280fb17e3456d763c9e3d19dbc1f2434b7",
    "zh:efdee845f5bf7cf9cb811f1b8a23515cc15d443787d93d6aeb50280b7fe354af",
  ]
}
