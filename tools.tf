module "listmonk" {
  providers = {
    scaleway = scaleway.scaleway_project
  }
  source              = "./tools/listmonk"
  kubeconfig          = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  scaleway_access_key = var.scaleway_project_config.access_key
  scaleway_secret_key = var.scaleway_project_config.secret_key

  hostname          = "listmonk.${var.prod_base_domain}"
  base_domain       = var.prod_base_domain
  monitoring_org_id = random_string.production_secret_org_id.result
  namespace         = "${var.project_slug}-listmonk"
}
